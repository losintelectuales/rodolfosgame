package com.my07lmk.game.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.my07lmk.game.game07lmk.Juego;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Juego07LMK";
		config.width = 800;
		config.height = 600;
		config.resizable=false;
		config.addIcon("icono.png", Files.FileType.Internal);
		new LwjglApplication(new Juego(), config);
	}
}
