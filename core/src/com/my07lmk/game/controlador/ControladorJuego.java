package com.my07lmk.game.controlador;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.my07lmk.game.modelo.Alien;
import com.my07lmk.game.modelo.Controles;
import com.my07lmk.game.modelo.ElementoMovil;
import com.my07lmk.game.modelo.Mundo;

import java.util.HashMap;

/**
 * Created by dam207 on 18/01/2016.
 */

public class ControladorJuego {
    Mundo mundo;
    private Alien alien;

    public ControladorJuego(Mundo mundo) {
        this.mundo = mundo;
        alien = mundo.getAlien();
    }

    public enum Keys {
        ESQUERDA,DEREITA,ARRIBA,ABAIXO
    }

    public HashMap<Keys, Boolean> keys = new HashMap<ControladorJuego.Keys, Boolean>();
    {
        keys.put(Keys.ESQUERDA, false);
        keys.put(Keys.DEREITA, false);
        keys.put(Keys.ARRIBA, false);
        keys.put(Keys.ABAIXO, false);
    };

    /**
     * Modifica o estado do mapa de teclas e pon a true
     * @param tecla: tecla pulsada
     */
    public void pulsarTecla(Keys tecla){
        keys.put(tecla, true);
    }
    /**
     * Modifica o estado do mapa de teclas e pon a false
     * @param tecla: tecla liberada
     */
    public void liberarTecla(Keys tecla){
        keys.put(tecla, false);
    }

    private void controlarAlien(float delta){

        // Actualiza Alien
        alien.update(delta);
        // Impide que se mova fora dos límites da pantalla
        if (alien.getPosicion().x <=0){
            alien.setPosicion(0, alien.getPosicion().y);
        }
        else {
            if (alien.getPosicion().x >= Mundo.TAMANO_MUNDO_ANCHO-alien.getTamano().x){
                alien.setPosicion(Mundo.TAMANO_MUNDO_ANCHO-alien.getTamano().x, alien.getPosicion().y);
            }

        }

        if (alien.getPosicion().y <= Controles.FONDO_NEGRO.height){
            alien.setPosicion(alien.getPosicion().x,Controles.FONDO_NEGRO.height);
        }
        else {
            if (alien.getPosicion().y >= Mundo.TAMANO_MUNDO_ALTO-alien.getTamano().y){
                alien.setPosicion(alien.getPosicion().x, Mundo.TAMANO_MUNDO_ALTO-alien.getTamano().y);
            }

        }

        // Controla que suba enriba dun elemento móvil
        alien.setVelocidade_montado(0);
        for (ElementoMovil elem : mundo.getRocas()){
            if (Intersector.overlaps(elem.getRectangulo(), alien.getRectangulo())){
                alien.setVelocidade_montado(elem.getVelocidade());
            }
        }
        for (ElementoMovil elem : mundo.getTroncos()){
            if (Intersector.overlaps(elem.getRectangulo(), alien.getRectangulo())){
                alien.setVelocidade_montado(elem.getVelocidade());
            }
        }

        // Controla se lle colle un vehículo
        for (ElementoMovil elem : mundo.getCoches()){
           if (Intersector.overlaps(elem.getRectangulo(), alien.getRectangulo())){
                // ALIEN MORTO
               alien.setNumVidas(Alien.TIPOS_VIDA.MUERTO);
                alien.inicializarAlien();
           }
       }

        // Controla se cae a auga ou lava
        if (alien.getVelocidade_montado()==0){
            boolean seguro=false;
            // Se está nunha zona segura xa non mira as perigosas
            for(int cont=0; cont < Mundo.ZONAS_SEGURAS.length;cont++){
                if (Intersector.overlaps(Mundo.ZONAS_SEGURAS[cont], alien.getRectangulo())){
                    seguro=true;
                    break;
                }
            }
            if (!seguro){
                for(int cont=0; cont < Mundo.ZONAS_PERIGOSAS.length;cont++){
                    if (Intersector.overlaps(Mundo.ZONAS_PERIGOSAS[cont], alien.getRectangulo())){
                        // ALIEN MORRE
                        alien.setNumVidas(Alien.TIPOS_VIDA.MUERTO);
                        alien.inicializarAlien();
                    }
                }
            }
        }

        // Chegar a nave
            if (Intersector.overlaps(mundo.getNave().getRectangulo(), alien.getRectangulo())){
                // Victoria
                alien.setNumVidas(Alien.TIPOS_VIDA.SALVADO);
                alien.inicializarAlien();
            }


    }

    private void controlarCoches(float delta) {

        for (ElementoMovil coche : mundo.getCoches()) {
            coche.update(delta);
            if (coche.getVelocidade() > 0) {   // Vai de esquerda a dereita
                if (coche.getPosicion().x >= Mundo.TAMANO_MUNDO_ANCHO) {
                    coche.setPosicion(-Mundo.TAMANO_AUTOBUSES.x, coche.getPosicion().y);
                }
            } else {   // Vai de dereita a esquerda
                if (coche.getPosicion().x <= -coche.getTamano().x) {
                    if (coche.getTipo() == ElementoMovil.TIPOS_ELEMENTOS.COCHE)       // E un coche enton necesitamos situalo un pouco a dereita se non vai pisando o autobus
                        coche.setPosicion(Mundo.TAMANO_MUNDO_ANCHO + Mundo.TAMANO_AUTOBUSES.x - Mundo.TAMANO_COCHES.x, coche.getPosicion().y);
                    else
                        coche.setPosicion(Mundo.TAMANO_MUNDO_ANCHO, coche.getPosicion().y);
                }
            }
        }


    }
    private void controlarRocas(float delta) {

        for (ElementoMovil rocas : mundo.getRocas()) {
            rocas.update(delta);
            if (rocas.getVelocidade() > 0) {   // Vai de esquerda a dereita
                if (rocas.getPosicion().x >= Mundo.TAMANO_MUNDO_ANCHO) {
                    rocas.setPosicion(-Mundo.TAMANO_ROCA.x, rocas.getPosicion().y);
                }
            } else {   // Vai de dereita a esquerda
                if (rocas.getPosicion().x <= -rocas.getTamano().x) {
                    // E un coche enton necesitamos situalo un pouco a dereita se non vai pisando o autobus
                    rocas.setPosicion(Mundo.TAMANO_MUNDO_ANCHO + Mundo.TAMANO_ROCA.x, rocas.getPosicion().y);
                }
            }

        }
    }
    private void controlarTroncos(float delta) {
        for (ElementoMovil tronco : mundo.getTroncos()) {
            tronco.update(delta);
            if (tronco.getPosicion().x < -Mundo.TAMANO_TRONCO.x) {
                mundo.getTroncos().add(new ElementoMovil(new Vector2(MathUtils.random(Mundo.TAMANO_MUNDO_ANCHO, Mundo.TAMANO_MUNDO_ANCHO + Mundo.TAMANO_TRONCO.x), tronco.getPosicion().y), Mundo.TAMANO_TRONCO.cpy(), tronco.getVelocidade(), ElementoMovil.TIPOS_ELEMENTOS.TRONCO));
                mundo.getTroncos().removeValue(tronco, true);

            }
            if (tronco.getPosicion().x > Mundo.TAMANO_MUNDO_ANCHO) {
                mundo.getTroncos().add(new ElementoMovil(new Vector2(MathUtils.random(-2 * Mundo.TAMANO_TRONCO.x, -Mundo.TAMANO_TRONCO.x),
                        tronco.getPosicion().y), Mundo.TAMANO_TRONCO.cpy(), tronco.getVelocidade(), ElementoMovil.TIPOS_ELEMENTOS.TRONCO));
                mundo.getTroncos().removeValue(tronco, true);
            }

        }
    }
    private void controlarNave(float delta){
        mundo.getNave().update(delta);
    }

    /**
     * Vai chamar a todos os métodos para mover e controlar os personaxes
     * Tamén xestionará os eventos producidos polo usuario e que veñen dende a clase PantallaXogo
     *
     * @param delta
     */
    public void update(float delta) {
        controlarCoches(delta);
        controlarTroncos(delta);
        controlarRocas(delta);
        controlarNave(delta);
        controlarAlien(delta);

        procesarEntradas();
    }

    private void procesarEntradas(){

        if (keys.get(Keys.DEREITA))
            alien.setVelocidadeX(alien.velocidade_max);
        if (keys.get(Keys.ESQUERDA))
            alien.setVelocidadeX(-alien.velocidade_max);
        if (!(keys.get(Keys.ESQUERDA)) && (!(keys.get(Keys.DEREITA))))
            alien.setVelocidadeX(0);

        if (keys.get(Keys.ARRIBA))
            alien.setVelocidadeY(alien.velocidade_max);
        if (keys.get(Keys.ABAIXO))
            alien.setVelocidadeY(-alien.velocidade_max);
        if (!(keys.get(Keys.ARRIBA)) && (!(keys.get(Keys.ABAIXO))))
            alien.setVelocidadeY(0);

    }
}
