package com.my07lmk.game.controlador;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.my07lmk.game.game07lmk.AssetsJuego;
import com.my07lmk.game.modelo.Alien;
import com.my07lmk.game.modelo.Alien2;
import com.my07lmk.game.modelo.Controles;
import com.my07lmk.game.modelo.Mundo;
import com.my07lmk.game.modelo.Mundo2;
import java.util.HashMap;

/**
 * Created by dam207 on 18/01/2016.
 */

public class ControladorJuegoGolden {
    Mundo2 mundo2;
    private Alien2 alien2;
    MapObjects collisionObjects;
    Rectangle rectobject;
    ShapeRenderer shaperender;



    public ControladorJuegoGolden(Mundo2 mundo2) {
        this.mundo2 = mundo2;
        alien2 = mundo2.getAlien();
//        objects = mundo2.getCollisionlayer().getObjects();
        shaperender=new ShapeRenderer();
    }

    public enum Keys {
        ESQUERDA,DEREITA,ARRIBA,ABAIXO
    }

    public HashMap<Keys, Boolean> keys = new HashMap<ControladorJuegoGolden.Keys, Boolean>();
    {
        keys.put(Keys.ESQUERDA, false);
        keys.put(Keys.DEREITA, false);
        keys.put(Keys.ARRIBA, false);
        keys.put(Keys.ABAIXO, false);
    };

    /**
     * Modifica o estado do mapa de teclas e pon a true
     * @param tecla: tecla pulsada
     */
    public void pulsarTecla(Keys tecla){
        keys.put(tecla, true);
    }
    /**
     * Modifica o estado do mapa de teclas e pon a false
     * @param tecla: tecla liberada
     */
    public void liberarTecla(Keys tecla){
        keys.put(tecla, false);
    }


    private void controlarAlien(float delta){

        // Actualiza Alien
        alien2.update(delta);


        collisionObjects=mundo2.getCollisionObjects();


        for (int i = 0; i < collisionObjects.getCount(); i++) {
            RectangleMapObject obj = (RectangleMapObject) collisionObjects.get(i);
            Rectangle rect = obj.getRectangle();


            if(rect.overlaps(alien2.getRectangulo())) {
                if(alien2.getVelocidadeX()>0){
                    alien2.setPosicion(alien2.getPosicion().x - 1, alien2.getPosicion().y);

                }
                if(alien2.getVelocidadeX()<0){
                    alien2.setPosicion(alien2.getPosicion().x + 1, alien2.getPosicion().y);

                }
                if(alien2.getVelocidadeY()>0){
                    alien2.setPosicion(alien2.getPosicion().x, alien2.getPosicion().y - 1);

                }
                if(alien2.getVelocidadeY()<0){
                    alien2.setPosicion(alien2.getPosicion().x, alien2.getPosicion().y + 1);

                }
            }

        }

    }



    /**
     * Vai chamar a todos os métodos para mover e controlar os personaxes
     * Tamén xestionará os eventos producidos polo usuario e que veñen dende a clase PantallaXogo
     *
     * @param delta
     */
    public void update(float delta) {

        controlarAlien(delta);
        procesarEntradas();
    }

    private void procesarEntradas(){

        if (keys.get(Keys.DEREITA)) {
            alien2.setVelocidadeX(alien2.velocidade_max);
        }
        if (keys.get(Keys.ESQUERDA)) {
            alien2.setVelocidadeX(-alien2.velocidade_max);
        }
        if (!(keys.get(Keys.ESQUERDA)) && (!(keys.get(Keys.DEREITA)))) {
            alien2.setVelocidadeX(0);
        }
        if (keys.get(Keys.ARRIBA)) {
            alien2.setVelocidadeY(alien2.velocidade_max);
        }

        if (keys.get(Keys.ABAIXO)) {
            alien2.setVelocidadeY(-alien2.velocidade_max);
        }
        if (!(keys.get(Keys.ARRIBA)) && (!(keys.get(Keys.ABAIXO)))) {
            alien2.setVelocidadeY(0);
        }
    }

}
