package com.my07lmk.game.pantallas07lmk;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.my07lmk.game.game07lmk.Juego;
import com.my07lmk.game.game07lmk.My07lmkGame;
import com.my07lmk.game.modelo.Mundo;

/**
 * Created by dam207 on 11/01/2016.
 */
public class PantallaPresentacion implements Screen, InputProcessor {
    private Juego juego;

    private OrthographicCamera camara2d;
    private SpriteBatch spritebatch;
    private static Texture fondo;
    private Rectangle botones[]=
            {new Rectangle(100, 268, 98, 32),new Rectangle(100, 235, 98, 32),
                    new Rectangle(100, 200, 98, 32)};

    public PantallaPresentacion(Juego juego){

        this.juego=juego;

        camara2d = new OrthographicCamera();
        spritebatch = new SpriteBatch();
        fondo = new Texture(Gdx.files.internal("GRAFICOS/LIBGDX_itin1_pantallapresentacion.png"));

    }

    @Override
    public void render(float delta) {
        // TODO Auto-generated method stub

        spritebatch.begin();

        spritebatch.draw(fondo,0,0, Mundo.TAMANO_MUNDO_ANCHO,Mundo.TAMANO_MUNDO_ALTO);

        spritebatch.end();


    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

        camara2d.setToOrtho(false, Mundo.TAMANO_MUNDO_ANCHO, Mundo.TAMANO_MUNDO_ALTO);
        camara2d.update();

        spritebatch.setProjectionMatrix(camara2d.combined);
        spritebatch.disableBlending();

    }

    @Override
    public void show() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(this);

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub
        // Neste caso non fai falta poñelo xa que imos ser nos o que chamemos a dispose cando cambiemos de pantalla.
        //dispose();

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(null);

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(this);

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(null);

        spritebatch.dispose();
        fondo.dispose();

    }

    @Override
    public boolean keyDown(int keycode) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        Vector3 temp = new Vector3(screenX,screenY,0);
        camara2d.unproject(temp);
        Circle dedo = new Circle(temp.x,temp.y,2);

        if (Intersector.overlaps(dedo, botones[0]))	{	// Pulsar Juego nuevo
            dispose();
          //  juego.setScreen(new PantallaJuego(juego));//////////////////////////////////////////////////////////////////////////////////////////////////////
            juego.setScreen(new PantallaGolden(juego));
        }

        if(Intersector.overlaps(dedo, botones[1])){//marcadores
            dispose();
           // juego.setScreen(new PantallaMarcadores(juego, this));

        }

        if(Intersector.overlaps(dedo, botones[2])){ //
                Gdx.app.exit();
            }




        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        // TODO Auto-generated method stub
        return false;
    }


}
