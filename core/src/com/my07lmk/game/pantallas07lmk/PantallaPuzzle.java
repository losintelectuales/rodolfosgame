package com.my07lmk.game.pantallas07lmk;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.my07lmk.game.controlador.ControladorJuegoGolden;
import com.my07lmk.game.game07lmk.Juego;
import com.my07lmk.game.modelo.Controles;
import com.my07lmk.game.modelo.Mundo2;
import com.my07lmk.game.renderer07lmk.RendererJuegoGolden;
import com.my07lmk.game.renderer07lmk.RendererJuegoGolden2;

/**
 * Created by kevin on 31/01/2016.
 */
public class PantallaPuzzle implements Screen, InputProcessor {
    Mundo2 mundo2;
    private Juego juego;
    private TiledMap tiledMap;


    private RendererJuegoGolden2 rendererJuegoGolden;
    private ControladorJuegoGolden controladorJuego;

    public PantallaPuzzle(Juego juego, Mundo2 mundo2) {
        this.juego=juego;
        this.mundo2=mundo2;
        mundo2.getAlien().setPosicion(32,34);
        tiledMap = new TmxMapLoader().load("pantalla2.tmx");
        mundo2.setTiledMap(tiledMap);
        rendererJuegoGolden = new RendererJuegoGolden2(mundo2);
        controladorJuego = new ControladorJuegoGolden(mundo2);
    }

    @Override
    public void render(float delta) {
        // TODO Auto-generated method stub
        controladorJuego.update(delta);
        rendererJuegoGolden.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub
        rendererJuegoGolden.resize(width, height);
    }

    @Override
    public void show() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(null);

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(this);

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
        Gdx.input.setInputProcessor(null);
        rendererJuegoGolden.dispose();
    }


    @Override
    public boolean keyDown(int keycode) {
        // TODO Auto-generated method stub
        // Liberamos as teclas para que se arrastramos o dedo a outro control sen soltar o anterior non xunte o efecto
        controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ABAIXO);
        controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ARRIBA);
        controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ESQUERDA);
        controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.DEREITA);

        switch (keycode) {
            case Input.Keys.UP:
                controladorJuego.pulsarTecla(ControladorJuegoGolden.Keys.ARRIBA);
                break;
            case Input.Keys.DOWN:
                controladorJuego.pulsarTecla(ControladorJuegoGolden.Keys.ABAIXO);
                break;
            case Input.Keys.LEFT:
                controladorJuego.pulsarTecla(ControladorJuegoGolden.Keys.ESQUERDA);
                break;
            case Input.Keys.RIGHT:
                controladorJuego.pulsarTecla(ControladorJuegoGolden.Keys.DEREITA);
                break;
        }


        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.UP:
                controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ARRIBA);
                break;
            case Input.Keys.DOWN:
                controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ABAIXO);
                break;
            case Input.Keys.LEFT:
                controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ESQUERDA);
                break;
            case Input.Keys.RIGHT:
                controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.DEREITA);
                break;
        }
        return false;
    }


    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector3 temporal = new Vector3(screenX, screenY, 0);
        this.rendererJuegoGolden.getCamara2d().unproject(temporal);
        Rectangle recTemporal = new Rectangle();

        Circle dedo = new Circle(temporal.x,temporal.y,2);


        if (Intersector.overlaps(new Circle(temporal.x, temporal.y, 2), Controles.FLECHA_IZQUIERDA)){
            controladorJuego.pulsarTecla(ControladorJuegoGolden.Keys.ESQUERDA);
        } else if (Intersector.overlaps(new Circle(temporal.x, temporal.y, 2), Controles.FLECHA_DERECHA)) {
            controladorJuego.pulsarTecla(ControladorJuegoGolden.Keys.DEREITA);
        } else if (Intersector.overlaps(new Circle(temporal.x, temporal.y, 2), Controles.FLECHA_ABAJO)) {
            controladorJuego.pulsarTecla(ControladorJuegoGolden.Keys.ABAIXO);
        } else if (Intersector.overlaps(new Circle(temporal.x, temporal.y, 2), Controles.FLECHA_ARRIBA)) {
            controladorJuego.pulsarTecla(ControladorJuegoGolden.Keys.ARRIBA);
        }

        recTemporal.set(Controles.CONTROL_PAUSE.x, Controles.CONTROL_PAUSE.y, Controles.CONTROL_PAUSE.width, Controles.CONTROL_PAUSE.height);
        if (Intersector.overlaps(dedo, recTemporal)){

        }

        recTemporal.set(Controles.CONTROL_SAIR.x,Controles.CONTROL_SAIR.y,Controles.CONTROL_SAIR.width,Controles.CONTROL_SAIR.height);
        if (Intersector.overlaps(dedo, recTemporal)){
            dispose();
            juego.setScreen(new PantallaPresentacion(juego));
        }

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ABAIXO);
        controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ARRIBA);
        controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.ESQUERDA);
        controladorJuego.liberarTecla(ControladorJuegoGolden.Keys.DEREITA);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}


