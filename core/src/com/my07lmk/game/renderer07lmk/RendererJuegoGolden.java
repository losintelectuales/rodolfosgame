package com.my07lmk.game.renderer07lmk;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.my07lmk.game.game07lmk.AssetsJuego;
import com.my07lmk.game.modelo.Alien;
import com.my07lmk.game.modelo.Alien2;
import com.my07lmk.game.modelo.Mundo;
import com.my07lmk.game.modelo.Mundo2;

/**
 * Created by dam207 on 11/01/2016.
 */
public class RendererJuegoGolden {

    /**
     * Debuxa todos os elementos gráficos da pantalla
     *
     * @param delta: tempo que pasa entre un frame e o seguinte.
     */
    private OrthographicCamera camara2d;
    OrthogonalTiledMapRenderer tiledMapRenderer;
    SpriteBatch spritebatch;
    private Alien2 alien2;
    MapObjects collisionObjects2;
    boolean entroPuerta=false;

    private Mundo2 mundo2;

    public boolean isEntroPuerta(){
        return entroPuerta;
    }
    public RendererJuegoGolden(Mundo2 mundo2) {


        this.mundo2 = mundo2;
        alien2 = mundo2.getAlien();
        camara2d = new OrthographicCamera();
        spritebatch = new SpriteBatch();
    }


    public void render(float delta) {
        collisionObjects2=mundo2.getPuerta();

        for (int i = 0; i < collisionObjects2.getCount(); i++) {
            RectangleMapObject obj = (RectangleMapObject) collisionObjects2.get(i);
            Rectangle rect = obj.getRectangle();


            if (rect.overlaps(alien2.getRectangulo())) {
                entroPuerta=true;
                System.out.println("Collision detectada");
            }
        }

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camara2d.setToOrtho(false, 800, 600);
        camara2d.translate(0, 350);
        camara2d.update();

        tiledMapRenderer = new OrthogonalTiledMapRenderer(mundo2.getTiledMap());
        tiledMapRenderer.setView(camara2d);
        alien2.update(delta);

        //accedemos al batch del tilemap render y lo iniciamos
        tiledMapRenderer.getBatch().begin();


        //renderizamos las capas que iran debajo de nuestro pj
        tiledMapRenderer.renderTileLayer((TiledMapTileLayer)mundo2.getTiledMap().getLayers().get("Capa base"));
        tiledMapRenderer.renderTileLayer((TiledMapTileLayer)mundo2.getTiledMap().getLayers().get("Suelo accesible debajo del personaje"));
        tiledMapRenderer.renderTileLayer((TiledMapTileLayer)mundo2.getTiledMap().getLayers().get("Suelo accesible debajo del personaje 2"));
        tiledMapRenderer.renderTileLayer((TiledMapTileLayer)mundo2.getTiledMap().getLayers().get("Suelo inaccesible debajo del personaje"));

        //renderizamos nuestro pj
        tiledMapRenderer.getBatch().draw(alien2.getCurrentFrame(), alien2.getPosicion().x, alien2.getPosicion().y);


        //Renderizamos las capas que se veran por encima
        tiledMapRenderer.renderTileLayer((TiledMapTileLayer)mundo2.getTiledMap().getLayers().get("Suelo accesible encima del personaje"));
        tiledMapRenderer.renderTileLayer((TiledMapTileLayer)mundo2.getTiledMap().getLayers().get("Suelo accesible encima del personaje 2"));
        tiledMapRenderer.renderTileLayer((TiledMapTileLayer)mundo2.getTiledMap().getLayers().get("Suelo inaccesible encima del personaje"));


        tiledMapRenderer.getBatch().end();

    }

    public void dispose() {

    }

    public void resize(int width, int height) {
        camara2d.setToOrtho(false, Mundo.TAMANO_MUNDO_ANCHO, Mundo.TAMANO_MUNDO_ALTO);
        camara2d.update();
        //spritebatch.setProjectionMatrix(camara2d.combined);
    }


    public OrthographicCamera getCamara2d() {
        return this.camara2d;
    }
}
