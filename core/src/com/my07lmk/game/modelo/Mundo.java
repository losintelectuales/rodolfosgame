package com.my07lmk.game.modelo;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by dam207 on 12/01/2016.
 */
public class Mundo {

    public static final int TAMANO_MUNDO_ANCHO=300;
    public static final int TAMANO_MUNDO_ALTO=500;
    private Array<ElementoMovil> coches;
    private Array<ElementoMovil>troncos;
    private Array<ElementoMovil> rocas;
    public final static Vector2 TAMANO_COCHES = new Vector2(20,15);
    public final static Vector2 TAMANO_AUTOBUSES = new Vector2(30,15);
    public final static Vector2 TAMANO_ROCA = new Vector2(60,60);
    public final static Vector2 TAMANO_TRONCO = new Vector2(80,40);

    public static final Rectangle ZONAS_PERIGOSAS[]=
            {new Rectangle(0,40,300,120),new Rectangle(0,220,300,120),
                    new Rectangle(0,420,300,80)};

    public static final Rectangle ZONAS_SEGURAS[]=
            {new Rectangle(40,420,20,60),new Rectangle(140,420,20,60),
                    new Rectangle(240,420,20,60)};  // AS PLATAFORMAS QUE ESTAN ENRIBA DA LAVA.


    private Alien alien;
    private Nave nave;

    public Mundo(){

        alien = new Alien(new Vector2(100,20), new Vector2(15,15),100);
        nave = new Nave(new Vector2(0,480), new Vector2(40,20),60);

        coches = new Array<ElementoMovil>();

        coches.add(new ElementoMovil(new Vector2(0,345),TAMANO_AUTOBUSES.cpy(),50,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(70,345),TAMANO_COCHES.cpy(),50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(95,345),TAMANO_COCHES.cpy(),50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(115,345),TAMANO_COCHES.cpy(),50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(140,345),TAMANO_AUTOBUSES.cpy(),50,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(210,345),TAMANO_AUTOBUSES.cpy(),50,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(240,345),TAMANO_COCHES.cpy(),50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(278,345),TAMANO_COCHES.cpy(),50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));

        coches.add(new ElementoMovil(new Vector2(40,380),TAMANO_COCHES.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(70,380),TAMANO_COCHES.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(105,380),TAMANO_COCHES.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(150,380),TAMANO_AUTOBUSES.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(180,380),TAMANO_AUTOBUSES.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(220,380),TAMANO_COCHES.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(265,380),TAMANO_AUTOBUSES.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));

        coches.add(new ElementoMovil(new Vector2(0,365),TAMANO_AUTOBUSES.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(70,365),TAMANO_COCHES.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(95,365),TAMANO_COCHES.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(115,365),TAMANO_COCHES.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(140,365),TAMANO_AUTOBUSES.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(210,365),TAMANO_AUTOBUSES.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(240,365),TAMANO_COCHES.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(278,365),TAMANO_COCHES.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.COCHE));

        coches.add(new ElementoMovil(new Vector2(40,400),TAMANO_COCHES.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(70,400),TAMANO_COCHES.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(105,400),TAMANO_COCHES.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(150,400),TAMANO_AUTOBUSES.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(180,400),TAMANO_AUTOBUSES.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));
        coches.add(new ElementoMovil(new Vector2(220,400),TAMANO_COCHES.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.COCHE));
        coches.add(new ElementoMovil(new Vector2(265,400),TAMANO_AUTOBUSES.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.AUTOBUS));


        rocas = new Array<ElementoMovil>();
        rocas.add(new ElementoMovil(new Vector2(40,100),TAMANO_ROCA.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.ROCA));
        rocas.add(new ElementoMovil(new Vector2(250,100),TAMANO_ROCA.cpy(),-35,ElementoMovil.TIPOS_ELEMENTOS.ROCA));
        rocas.add(new ElementoMovil(new Vector2(120,40),TAMANO_ROCA.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.ROCA));
        rocas.add(new ElementoMovil(new Vector2(290,40),TAMANO_ROCA.cpy(),35,ElementoMovil.TIPOS_ELEMENTOS.ROCA));

        troncos = new Array<ElementoMovil>();
        troncos.add(new ElementoMovil(new Vector2(100,220),TAMANO_TRONCO.cpy(),-50,ElementoMovil.TIPOS_ELEMENTOS.TRONCO));
        troncos.add(new ElementoMovil(new Vector2(60,260),TAMANO_TRONCO.cpy(),40,ElementoMovil.TIPOS_ELEMENTOS.TRONCO));
        troncos.add(new ElementoMovil(new Vector2(150,300),TAMANO_TRONCO.cpy(),-70,ElementoMovil.TIPOS_ELEMENTOS.TRONCO)); }


    public Alien getAlien() {return alien;}
    public Nave getNave() {
        return nave;
    }
    public Array<ElementoMovil>getCoches(){
        return coches;
    }
    public Array<ElementoMovil> getTroncos() {
        return troncos;
    }
    public Array<ElementoMovil> getRocas() {
        return rocas;
    }
}
