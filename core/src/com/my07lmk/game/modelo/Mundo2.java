package com.my07lmk.game.modelo;

import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;



/**
 * Created by kevin on 26/01/2016.
 */
public class Mundo2{
public static final int TAMANO_MUNDO_ANCHO=300;
public static final int TAMANO_MUNDO_ALTO=500;
    private TiledMapTileLayer collisionlayer;
    private MapObjects collisionObjects;
    private MapObjects collisionObjects2;
    private TiledMap tiledMap;


    private Alien2 alien2;

    public Mundo2() {
        alien2 = new Alien2(new Vector2(300, 600), new Vector2(16, 16), 30);
    }

    public void setTiledMap(TiledMap tiledMap) {
        this.tiledMap=tiledMap;
        collisionObjects = tiledMap.getLayers().get("Capa de Objetos 1").getObjects();
        collisionObjects2 = tiledMap.getLayers().get("Capa de Objetos 2").getObjects();
        }


    public MapObjects getCollisionObjects() {

        return collisionObjects;
    }
    public MapObjects getPuerta() {

        return collisionObjects2;
    }

    public Alien2 getAlien() {
        return alien2;
    }

    public TiledMap getTiledMap() {
        return tiledMap;
    }


}
