package com.my07lmk.game.modelo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.my07lmk.game.game07lmk.AssetsJuego;


/**
 * Created by dam207 on 18/01/2016.
 */
public class Alien2 extends Personaje {
    public static enum TIPOS_VIDA {INICIAL, SALVADO, MUERTO}
    private Array<TIPOS_VIDA> numVidas;
    private Vector2 velocidade;
    private float velocidadeMontado;
    private float stateTime;
    private String posicion="arriba";
    public enum Status {derecha, izquierda, arriba, abajo, quieto}

    protected TextureRegion currentFrame, quietoArriba, quitoAbajo, quietoDerecha, quietoIzquierda;
    protected Animation animacionDerecha, animacionIzquierda, animacionArriba, animacionAbajo;

    Status status;

    public Alien2(Vector2 posicion, Vector2 tamano, float velocidade_max) {
        super(posicion, tamano, velocidade_max);
      //  numVidas = new Array<Alien2.TIPOS_VIDA>();
        velocidade = new Vector2(0, 0);
        setVelocidade_montado(0);
        getRectangulo().setSize(tamano.x);
       // getRectangulo().setSize(tamano.x / 2);
       // setStatus(Status.quieto);



        animacionDerecha = new Animation(0.25f, new TextureRegion[]{
                new Sprite(AssetsJuego.textureDerecha1), new Sprite(AssetsJuego.textureDerecha2)
        });

        animacionIzquierda = new Animation(0.25f, new TextureRegion[]{
                new Sprite(AssetsJuego.textureIzquierda1), new Sprite(AssetsJuego.textureIzquierda2)
        });

        animacionArriba = new Animation(0.25f, new TextureRegion[]{
                new Sprite(AssetsJuego.textureArriba1), new Sprite(AssetsJuego.textureArriba2)
        });

        animacionAbajo = new Animation(0.25f, new TextureRegion[]{
                new Sprite(AssetsJuego.textureAbajo1), new Sprite(AssetsJuego.textureAbajo2)
        });

        quietoArriba=  new TextureRegion(AssetsJuego.textureArriba1);
        quitoAbajo=  new TextureRegion(AssetsJuego.textureAbajo1);
        quietoDerecha=  new TextureRegion(AssetsJuego.textureDerecha1);
        quietoIzquierda=  new TextureRegion(AssetsJuego.textureIzquierda1);

        currentFrame= quietoArriba;



        /*Vector temporal con todas las tectureRegion de la imagen;
        TextureRegion[][] tmp = TextureRegion.split(AssetsJuego.textureAnimationAlien,
                AssetsJuego.textureAnimationAlien.getWidth() / FRAME_COLS,
                AssetsJuego.textureAnimationAlien.getHeight() / FRAME_ROWS);


        walkFrames = new TextureRegion[FRAME_COLS];
        for (int j = 0; j < FRAME_COLS; j++) {
            walkFrames[j] = tmp[0][j];
        }

        idleFrame = tmp[0][1];

        animacionDerecha = new Animation(0.4f, walkFrames);
        animacionIzquierda = new Animation(0.2f, walkFrames);
        animacionArriba = new Animation(0.2f, walkFrames);
        animacionAbajo = new Animation(0.2f, walkFrames);


        currentAnimation = walkAnimation;
*/
    }

    @Override
    public void update(float delta) {
        // TODO Auto-generated method stub
        setPosicion(getPosicion().x + (velocidade.x + velocidadeMontado) * delta,
                getPosicion().y + velocidade.y * delta);



        //tiempo para cargar el frame que proceda de la animacion
        stateTime+=delta;

        if (velocidade.x == 0 && velocidade.y > 0) {
            // aliensube
            currentFrame=animacionArriba.getKeyFrame(stateTime, true);
            posicion="arriba";

        } else if (velocidade.x == 0 && velocidade.y < 0) {
            //alienbaja
            currentFrame=animacionAbajo.getKeyFrame(stateTime, true);
            posicion="abajo";
        } else if (velocidade.x > 0 && velocidade.y == 0) {
            //alien derecha
            currentFrame=animacionDerecha.getKeyFrame(stateTime, true);
            posicion="derecha";
        } else if (velocidade.x < 0 && velocidade.y == 0) {
            //alien izquierda
            currentFrame=animacionIzquierda.getKeyFrame(stateTime, true);
            posicion="izquierda";

        }else if(velocidade.x == 0 && velocidade.y == 0) {
            if(posicion.equalsIgnoreCase("arriba")){
                currentFrame=quietoArriba;
            }else if(posicion.equalsIgnoreCase("abajo")){
                currentFrame=quitoAbajo;
            }else if(posicion.equalsIgnoreCase("izquierda")){
                currentFrame=quietoIzquierda;
            }else if(posicion.equalsIgnoreCase("derecha")){
                currentFrame=quietoDerecha;
            }else{
                currentFrame=quietoArriba;
            }
        }




    }

    public void inicializarAlien() {
        setPosicion(100, 200);
        setVelocidade_montado(0);
        setVelocidadeX(0);
        setVelocidadeY(0);
        setTamano(15, 15);
        getRectangulo().setSize(tamano.x / 2);
    }


    public float getVelocidadeX() {
        return velocidade.x;
    }

    public float getVelocidadeY() {
        return velocidade.y;
    }

    public void setVelocidadeX(float x) {
        velocidade.x = x;

    }

    public void setVelocidadeY(float y) {
        velocidade.y = y;
    }

    public float getVelocidade_montado() {
        return velocidadeMontado;
    }

    public void setVelocidade_montado(float velocidade_montado) {
        this.velocidadeMontado = velocidade_montado;
    }



    @Override
    public void actualizarRectangulo() {

        getRectangulo().x = getPosicion().x + getTamano().x / 4;
        getRectangulo().y = getPosicion().y + getTamano().y / 4;

    }


    public TextureRegion getCurrentFrame() {
        return currentFrame;
    }

   /* public void setStatus(Status status) {
        if (this.status != status) {
            this.status = status;
            switch (this.status) {
                case derecha:
                    currentAnimation = animacionDerecha;
                    break;
                case izquierda:
                    currentAnimation = animacionIzquierda;
                    break;
                case arriba:
                    currentAnimation = animacionArriba;
                    break;
                case abajo:
                    currentAnimation = animacionAbajo;
                    break;
                case quieto:
                    currentAnimation=animacionQuieto;
                    break;
            }
        }
    }
*/

}
