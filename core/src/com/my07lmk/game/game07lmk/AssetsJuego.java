package com.my07lmk.game.game07lmk;

/**
 * Created by dam207 on 13/01/2016.
 */
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;

public class AssetsJuego {

    public static Texture textureArriba1, textureArriba2, textureAbajo1, textureAbajo2,
    textureDerecha1, textureDerecha2,textureIzquierda1, textureIzquierda2;

    /**
     * MÃ©todo encargado de cargar todas as texturas
     */
    public static void cargarTexturas(){

        FileHandle imageFileHandle1 = Gdx.files.internal("GRAFICOS/arriba1.png");
        textureArriba1 = new Texture(imageFileHandle1);

        FileHandle imageFileHandle2 = Gdx.files.internal("GRAFICOS/arriba2.png");
        textureArriba2 = new Texture(imageFileHandle2);

        FileHandle imageFileHandle3 = Gdx.files.internal("GRAFICOS/abajo1.png");
        textureAbajo1 = new Texture(imageFileHandle3);

        FileHandle imageFileHandle4 = Gdx.files.internal("GRAFICOS/abajo2.png");
        textureAbajo2 = new Texture(imageFileHandle4);

        FileHandle imageFileHandle5 = Gdx.files.internal("GRAFICOS/derecha1.png");
        textureDerecha1 = new Texture(imageFileHandle5);

        FileHandle imageFileHandle6 = Gdx.files.internal("GRAFICOS/derecha2.png");
        textureDerecha2 = new Texture(imageFileHandle6);

        FileHandle imageFileHandle7 = Gdx.files.internal("GRAFICOS/izquierda1.png");
        textureIzquierda1 = new Texture(imageFileHandle7);

        FileHandle imageFileHandle8 = Gdx.files.internal("GRAFICOS/izquierda2.png");
        textureIzquierda2 = new Texture(imageFileHandle8);
    }


    /**
     * MÃ©todo encargado de liberar todas as texturas
     */
    public static void liberarTexturas(){

    }
}
