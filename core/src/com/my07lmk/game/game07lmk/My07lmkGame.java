package com.my07lmk.game.game07lmk;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class My07lmkGame extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	private FPSLogger fps;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
		Utiles.imprimirLog("Juego", "create", "07LMK");
		fps = new FPSLogger();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
		//fps.log();
	}

	@Override
	public void dispose() {
		Utiles.imprimirLog("Juego", "create", "07LMK");
		super.dispose();
		Utiles.imprimirLog("My07lmkGame", "DISPOSE", "DISPOSE");
	}              // Chamado cando se libera. Antes sempre se chama a pause.

	@Override
	public void resize(int width, int height) {
		Utiles.imprimirLog("Juego", "create", "07LMK");
		// Chamado se cambia a resolución da pantalla (tamén se chama unha vez despois do create)
	}

	@Override
	public void pause() {
		Utiles.imprimirLog("Juego", "create", "07LMK");
		// Chamado ó saír da aplicación.
	}                //  En Android cando se preme o boton Home ou ten unha chamada. Bo sitio para gardar o estado do xogo.

	@Override
	public void resume() {
		Utiles.imprimirLog("Juego", "create", "07LMK");
	}             // Chamado ó reanudar ó xogo (volvemos ó xogo en Android ou vimos de minimizar o xogo en PC)

}
