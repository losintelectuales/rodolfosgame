package com.my07lmk.game.game07lmk;

import com.badlogic.gdx.Game;
import com.my07lmk.game.pantallas07lmk.PantallaPresentacion;

/**
 * Created by dam207 on 11/01/2016.
 */
public class Juego extends Game{


    @Override
    public void create() {
        AssetsJuego.cargarTexturas();
        setScreen(new PantallaPresentacion(this));
        //setScreen(new PantallaJuego(this));
    }

    @Override
    public void dispose(){
        super.dispose();
        AssetsJuego.liberarTexturas();
    }
}
